<?php

namespace Drupal\role_terms\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class RoleTermsRedirectSubscriber.
 *
 * @package Drupal\role_terms\EventSubscriber
 * redirects users given a checked role until role agreements are accepted.
 */
class RoleTermsRedirectSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // This announces which events you want to subscribe to.
    // We only need the request event for this example.  Pass
    // this an array of method names.
    return([
      KernelEvents::REQUEST => [
        ['redirectMyContentTypeNode'],
      ]
    ]);
  }

  /**
   * Redirect requests to force redirect to accept role.
   */
  public function redirectMyContentTypeNode(GetResponseEvent $event) {
    $request = $event->getRequest();

    // This is necessary because this also gets called on
    // node sub-tabs such as "edit", "revisions", etc.  This
    // prevents those pages from redirected.
    if ($request->attributes->get('_route') !== 'entity.node.canonical') {
      return;
    }

    // Only redirect a certain content type.
    $userCurrent = \Drupal::currentUser();
    $roles = $userCurrent->getRoles();
    $userCurrentId = $userCurrent->id();
    $database = \Drupal::database();
    foreach ($roles as $role) {
      $check_role = \Drupal::config('roleterms.adminsettings')->get($role . '_checkbox');
      $query = $database->select('role_terms', 'rt');
      $query->condition('rt.uid', $userCurrentId);
      $query->condition('rt.role_name', $role, '=');
      $query->fields('rt', ['role_name']);
      $result = $query->execute();
      $num_rows = $query->countQuery()->execute()->fetchField();
      // $redirect_url = Url::fromUri('entity:node/42');
      if ($num_rows == 0 && $check_role) {
        $redirect_url = '/roleterms/form?role=' . $role;
      }
    }
    if (!isset($redirect_url)) {
      return;
    }
    $response = new RedirectResponse($redirect_url, 301);
    $event->setResponse($response);
  }

}
