<?php

namespace Drupal\role_terms\Controller;

use Drupal\user\Entity\User;

/**
 * Provides route responses for the Role Terms Pages.
 */
class MyRolesTermsController {

  /**
   * Returns a sHPimple hello world page.
   *
   * @return array
   *   A very simple renderable array is returned
   *   Example usage rolesterms/agreement?role=anonymous.
   */
  public function myRolesTerms() {
    $database = \Drupal::database();
    $query = $database->select('role_terms', 'rt');
    $query->fields('rt', ['id', 'role_name', 'uid', 'accepted_date', 'status']);
    $results = $query->execute();

    $header = ['Username / Email', 'Role Name', 'Accepted Date'];
    $rows = [];
    foreach ($results as $row) {
      $user = User::load($row->uid);
      $rows[] = [
        [
          'data' => $user->getUsername() . ' / ' . $user->mail->value,
          'class' => 'email',
        ],
        ['data' => $row->role_name, 'class' => 'role-name'],
        ['data' => $row->accepted_date, 'class' => 'accepted-date'],
      ];
    }
    $element['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['id' => 'datatableid'],
    ];
    $element['table']['#attached']['library'][] = 'role_terms/datatables';
    return $element;
  }

}
