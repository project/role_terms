<?php

namespace Drupal\role_terms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class providing a form to users to accept terms by role.
 */
class RoleTermsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_terms_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $role_value = \Drupal::request()->query->get('role');
    $agreement_message = \Drupal::config('roleterms.adminsettings')->get($role_value);

    $element = [
      '#markup' => $role_value . ' agreement: ' . $agreement_message,
    ];

    $form['agreementtext']['#markup'] = t('@message', ['@message' => $agreement_message]);

    $form['accept_terms'] = [
      '#type' => 'checkbox',
      '#title' => 'Accept the terms and conditions',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the form values.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    $agreement_acceptance_status = $form_state->getValue('accept_terms');
    if ($agreement_acceptance_status == 1) {
      $userid = \Drupal::currentUser()->id();
      $role_name = \Drupal::request()->query->get('role');

      $database = \Drupal::database();
      $date = date('Y-m-d H:i:s');
      $result = $database->insert('role_terms')
        ->fields([
          'role_name' => $role_name,
          'uid' => $userid,
          'accepted_date' => $date,
          'status' => 1,
        ])
        ->execute();
      $messenger->addMessage('thanks for submitting your acceptance of your new roles terms and conditions');
      $url = Url::fromUserInput('/user');
      $form_state->setRedirectUrl($url);
    }
    else {
      $messenger->addMessage('thanks for submitting the form! Sorry but there was an error please contact an administrator');
    }
  }

}
