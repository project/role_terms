<?php

namespace Drupal\role_terms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The controller for the Role Terms Settings form.
 */
class RoleTermsConditionsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'roleterms.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'roleterms_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('roleterms.adminsettings');

    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();

    $form['checkbox_header']['#markup'] = t('<h3>Choose which Roles to Force Terms and Conditons to be accepted for:</h3>');

    foreach ($roles as $role) {
      $form[$role->id() . '_checkbox'] = [
        '#type' => 'checkbox',
        '#title' => $role->label(),
        '#default_value' => $config->get($role->id() . '_checkbox'),
      ];
    }

    foreach ($roles as $role) {
      $form[$role->id()] = [
        '#type' => 'textarea',
        '#title' => $this->t('Terms and Conditions for @role', ['@role' => $role->label()]),
        '#description' => $this->t('Enter the terms and conditons for the role @role', ['@role' => $role->label()]),
        '#default_value' => $config->get($role->id()),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    foreach ($roles as $role) {
      $this->config('roleterms.adminsettings')
        ->set($role->id(), $form_state->getValue($role->id()))
        ->save();
      $this->config('roleterms.adminsettings')
        ->set($role->id() . '_checkbox', $form_state->getValue($role->id() . '_checkbox'))
        ->save();
    }
    parent::submitForm($form, $form_state);
  }

}
