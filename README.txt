INTRODUCTION
------------

This project makes it so if a user is given a new role, they are upon next logged in page load given terms and conditions they must accept to receive the new role and super powers within the Drupal site. If they reject the terms and conditions they are then not given the role.

REQUIREMENTS
------------
No requirements, just a copy of Drupal 8 

INSTALLATION
------------

- Install the module as you would any Drupal module.

CONFIGURATION
-------------
Go to admin/config/rolebasedterms/adminsettings and setup the configuration,
then all users will be redirected to accept the role based agreements for
the roles being checked.
